<?php 

class TrackModel{

    private $table = 'track'; 
    private $db;

    public function __construct(){
        $this->db = new Database; 
    }

    public function getAllTrack(){ 
        $this->db->query("SELECT track.*, tema.namaTema FROM " . $this->table . " JOIN tema ON tema.id = track.id_tema");
        return $this->db->resultSet(); 
    }

    public function getTrackById($id)
	{
		$this->db->query('SELECT * FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id',$id);
		return $this->db->single();
    }

    public function tambahTrack($data){
        
        $ekstensi_diperbolehkan	= array('png','jpg','jpeg');
        $nama = $_FILES['img']['name'];
        $x = explode('.', $nama);
        $ekstensi = strtolower(end($x));
        $ukuran	= $_FILES['img']['size'];
        $file_tmp = $_FILES['img']['tmp_name'];	

        $track = array($data);
        $track[0]['img'] = $nama;
        
        move_uploaded_file($_FILES['img']['tmp_name'], '../img/'.$nama);
        


        $query = "INSERT INTO track(namaTrack, id_tema, jarak, harga, img) VALUES (:namaTrack, :id_tema, :jarak, :harga, :img)";
        $this->db->query($query); 
        
        $this->db->bind('namaTrack', $data['namaTrack']);
        
        $this->db->bind('id_tema', $data['id_tema']);
        $this->db->bind('jarak', $data['jarak']);
        $this->db->bind('harga', $data['harga']);
        $this->db->bind('img', $nama);
        
        $this->db->execute();
       
        return $this->db->rowCount();
    }

    public function deleteTrack($id)
	{
		$this->db->query('DELETE FROM ' . $this->table . ' WHERE id=:id');
		$this->db->bind('id',$id);
		$this->db->execute();

		return $this->db->rowCount();
    }

    public function cariTrack()
	{
		$key = $_POST['key'];
		$this->db->query("SELECT * FROM " . $this->table . " WHERE namaTrack LIKE :key");
		$this->db->bind('key',"%$key%");
		return $this->db->resultSet();
	}

}