<?php

class LoginModel {
	
	private $table = 'user';
	private $db;

	public function __construct()
	{
		$this->db = new Database;
	}

	public function checkLogin($data)
	{
		$query = "SELECT * FROM user WHERE username = :username AND password = :password";
		$this->db->query($query);
		$this->db->bind('username', $data['username']);
		$this->db->bind('password', md5($data['password']));
		//$this->db->execute();
		//return $this->db->rowCount();
		//$data = $query->fetch();

		$data =  $this->db->single();
		return $data;
	}

	public function tambahUser($data)
	{	
		
		$query = "INSERT INTO user(nama,username,password,level) VALUES(:nama,:username,:password,:level)";
		$this->db->query($query);
		$this->db->bind('nama',$data['nama']);
		$this->db->bind('username',$data['username']);
		$this->db->bind('password', md5($data['password']));
		$this->db->bind('level', $data['level']);
		$this->db->execute();

		$id = $this->db->insert_id();

		$query = "INSERT INTO data_diri (no_telp,gender,umur,alamat,id_user) VALUES(:no_telp,:gender,:umur,:alamat,:id_user)";
		$this->db->query($query);
		$this->db->bind('no_telp',$data['no_telp']);
		$this->db->bind('gender',$data['gender']);
		$this->db->bind('umur', $data['umur']);
		$this->db->bind('alamat',$data['alamat']);
		$this->db->bind('id_user', $id);
		$this->db->execute();

		return $this->db->rowCount();
	}



}