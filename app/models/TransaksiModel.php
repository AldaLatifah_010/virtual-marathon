<?php 

class TransaksiModel{

    private $table = 'transaksi'; //variabel dengan sifat private, supaya variabel ini ga digunakan di tempat lain
    private $db;

    public function __construct(){
        $this->db = new Database; // koneksi ke database
    }

    public function getAllTransaksi(){
        $this->db->query("SELECT * FROM " . $this->table . 
                            "INNER JOIN track ON track.id = transaksi.id_track
                             INNER JOIN user ON user.id = transaksi.id_user");
        return $this->db->resultSet();
    }


}