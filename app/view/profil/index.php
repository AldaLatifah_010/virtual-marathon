<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="<?= base_url ?>/assets/css/profilPerusahaan.css">
</head>
<body class="body">

<nav class="header">
	<img src="<?= base_url ?>/assets/img/logo.png" style="width: 26.375px; height: 36.9375px">
	<a style="padding: 10px; color: white; margin-top: 5px">VirMarathon.</a>
	<button type="submit" class="signin" style="margin-left: 1050px"><a href="<?= base_url ?>/login">Sign in</a></button>
    <button type="submit" class="signup"><a href="<?= base_url ?>/register">Sign up</a></button>
</nav>
<div class="slideshow-container">
	<div class="mySlides fade">
		<img src="<?= base_url ?>/assets/img/gunung bromo.jpeg" style="width:100%">
		<div class="text">Gunung Bromo</div>
	</div>

	<div class="mySlides fade">
		<img src="<?= base_url ?>/assets/img/gunung rinjani.jpeg" style="width:100%">
		<div class="text">Gunung Rinjani</div>
	</div>

	<div class="mySlides fade">
		<img src="<?= base_url ?>/assets/img/pantai ora.jpeg" style="width:100%">
		<div class="text">Pantai Ora</div>
	</div>

	<div class="mySlides fade">
		<img src="<?= base_url ?>/assets/img/pantai parangtritis.jpeg" style="width:100%">
		<div class="text">Pantai Parangtritis</div>
	</div>

	<div class="mySlides fade">
		<img src="<?= base_url ?>/assets/img/sungai mahakam.jpeg" style="width:100%">
		<div class="text">Sungai Mahakam</div>
	</div>

	<div class="mySlides fade">
		<img src="<?= base_url ?>/assets/img/sungai musi.jpeg" style="width:100%">
		<div class="text">Sungai Musi</div>
	</div>

	<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
	<a class="next" onclick="plusSlides(1)">&#10095;</a>

</div>
<br>

<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
  <span class="dot" onclick="currentSlide(4)"></span> 
  <span class="dot" onclick="currentSlide(5)"></span> 
  <span class="dot" onclick="currentSlide(6)"></span> 
</div>
<br>

<div class="card-body">
    <p>Halo! <br>
       Selamat datang di VirMarathon! <br>
       VirMarathon merupakan sebuah acara kegiatan marathon secara online dengan berbagai pilihan jarak. 
       Marathon dilaksanakan secara virtual dengan berbagai jenis tema seperti pegunungan, sungai, dan pantai.
       Untuk lokasi rute dari marathon virtual ini dapat ditentukan sendiri oleh peserta dengan jarak yang 
       sudah ditentukan berdasarkan tema dan track yang dipilih sendiri oleh peserta. <br>
       Semua peserta akan mendapatkan medali VirMarathon. setelah menyelesaikan marathon virtual sesuai dengan track yang dipilih peserta!
    </p>
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}

</script>

</body>
</html> 
