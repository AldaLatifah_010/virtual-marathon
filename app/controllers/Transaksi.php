<?php

class Transaksi extends Controller{

    public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	}

    public function index(){ 
        $data['title'] = "Data Transaksi"; 
        $data['progress'] = $this->model('TransaksiModel')->getAllTransaksi(); 
        $this->view('templates/header', $data); 
        $this->view('templates/sidebar', $data);
        $this->view('progress/index',$data);
        $this->view('templates/footer', $data);
    }

    public function tambah(){ 
        $data['title'] = "Tambah Data Transaksi"; 
        $data['track'] = $this->model('TrackModel')->getAllTrack();

        $this->view('templates/header', $data);
        $this->view('templates/sidebar', $data);
        $this->view('progress/tambah', $data);
        $this->view('templates/footer', $data);
    }

    
	public function edit($id){ 
		$data['title'] = 'Edit Transaksi'; 
        $data['transaksi'] = $this->model('TransaksiModel')->getTranskasiById($id); 
        
        $this->view('templates/header', $data);
		$this->view('templates/header', $data); 
		$this->view('templates/sidebar', $data);
		$this->view('transaksi/edit', $data);
		$this->view('templates/footer');
    }

    public function cari()
	{
		$data['title'] = 'Data Transaksi';
		$data['transaksi'] = $this->model('TransaksiModel')->cariTransaksi();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('transaksi/index', $data);
		$this->view('templates/footer');
	}

    public function simpanData(){
        if($this->model('TransaksiModel')->tambahTransaksi($_POST) > 0 ){
            Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); 
            header('Location:'.base_url.'/transaksi' ); 
            exit;
        }else{
            Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); 
            header('Location:'.base_url.'/transaksi' ); 
            exit;
        }
    }

    public function updateTransaksi(){	

        if( $this->model('TransaksiModel')->updateDataTransaksi($_POST) > 0 ) {
            Flasher::setMessage('Berhasil','diupdate','success');
            header('Location: '. base_url . '/transaksi');
            exit;			
        }else{
            Flasher::setMessage('Gagal','diupdate','danger');
            header('Location: '. base_url . '/transaksi');
            exit;	
        }
    }

    public function hapus($id){
		if( $this->model('TransaksiModel')->deleteTransaksi($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('Location: '. base_url . '/transaksi');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('Location: '. base_url . '/transaksi');
			exit;	
		}
	}

}