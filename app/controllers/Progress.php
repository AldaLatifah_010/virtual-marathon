<?php

class Progress extends Controller{

    public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	}

    public function index(){ //function tampil data
        $data['title'] = "Data Progress"; // ini untuk title di desainnya
        $data['progress'] = $this->model('ProgressModel')->getAllProgress(); //ngambil function getAllTema dari ProgressModel
        //ngereturn/nampilin header,index, footer,sidebar saat kita ngejalankan fungsi index
        $this->view('templates/header', $data); 
        $this->view('templates/sidebar', $data);
        $this->view('progress/index',$data);
        $this->view('templates/footer', $data);
    }

    public function tambah(){ //function tampil halaman tambah
        $data['title'] = "Tambah Data Progress"; //title untuk desainnya
        $data['progress'] = $this->model('ProgressModel')->getAllProgress(); //ngambil function getAllProgress dari ProgressModel
        //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
        $this->view('templates/header', $data);
        $this->view('templates/sidebar', $data);
        $this->view('progress/tambah', $data);
        $this->view('templates/footer', $data);
    }

    
	public function edit($id){ //function halaman edit
		$data['title'] = 'Edit Progress'; //title untuk desain halamannya
        $data['progress'] = $this->model('ProgressModel')->getProgressById($id); // ngambil function getTemaById dari model
        //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
        $this->view('templates/header', $data);
		$this->view('templates/header', $data); 
		$this->view('templates/sidebar', $data);
		$this->view('progress/edit', $data);
		$this->view('templates/footer');
    }

    public function cari()
	{
		$data['title'] = 'Data Progress';
		$data['progress'] = $this->model('ProgressModel')->cariProgress();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('progress/index', $data);
		$this->view('templates/footer');
	}

    public function simpanData(){
        if($this->model('ProgressModel')->tambahProgress($_POST) > 0 ){
            Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); //notifikasi
            header('Location:'.base_url.'/progress' ); //redirect ke halaman localhost/marathon/public/progress
            exit;
        }else{
            Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); //notifikasi
            header('Location:'.base_url.'/progress' ); //redirect ke halaman
            exit;
        }
    }

    public function updateProgress(){	

        if( $this->model('ProgressModel')->updateDataProgress($_POST) > 0 ) {
            Flasher::setMessage('Berhasil','diupdate','success');
            header('Location: '. base_url . '/progress');
            exit;			
        }else{
            Flasher::setMessage('Gagal','diupdate','danger');
            header('Location: '. base_url . '/progress');
            exit;	
        }
    }

    public function hapus($id){
		if( $this->model('ProgressModel')->deleteProgress($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('Location: '. base_url . '/progress');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('Location: '. base_url . '/progress');
			exit;	
		}
	}

}