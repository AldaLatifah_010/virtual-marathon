<?php

class Tema extends Controller{

    public function __construct()
	{	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/login');
			exit;
		}
	}

    public function index(){ //function tampil data
        $data['title'] = "Data Tema"; // ini untuk title di desainnya
        $data['tema'] = $this->model('TemaModel')->getAllTema();
        $data['level'] = $_SESSION['level']; //ngambil function getAllTema dari TemaModel
        //ngereturn/nampilin header,index, footer,sidebar saat kita ngejalankan fungsi index
        $this->view('templates/header', $data); 
        $this->view('tema/index',$data);
    }

    public function tambah(){ //function tampil halaman tambah
        $data['title'] = "Tambah Data Tema"; //title untuk desainnya
        $data['tema'] = $this->model('TemaModel')->getAllTema(); //ngambil function getAllTema dari TemaModel
        //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
        $this->view('templates/header', $data);
        $this->view('templates/sidebar', $data);
        $this->view('tema/tambah', $data);
        $this->view('templates/footer', $data);
    }

    
	public function edit($id){ //function halaman edit
		$data['title'] = 'Edit Tema'; //title untuk desain halamannya
        $data['tema'] = $this->model('TemaModel')->getTemaById($id); // ngambil function getTemaById dari model
        //ngereturn header, tambahTema, sidebar, footer saat jalannya fungsi tambah
        $this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('tema/edit', $data);
		$this->view('templates/footer');
    }

    public function cari()
	{
		$data['title'] = 'Data Tema';
		$data['tema'] = $this->model('TemaModel')->cariTema();
		$data['key'] = $_POST['key'];
		$this->view('templates/header', $data);
		$this->view('templates/sidebar', $data);
		$this->view('tema/index', $data);
		$this->view('templates/footer');
	}

    

    public function simpanData(){
        if($this->model('TemaModel')->tambahTema($_POST) > 0 ){
            Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); //notifikasi
            header('Location:'.base_url.'/tema' ); //redirect ke halaman localhost/marathon/public/tema
            exit;
        }else{
            Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); //notifikasi
            header('Location:'.base_url.'/tema' ); //redirect ke halaman
            exit;
        }
    }

    public function updateTema(){	

        if( $this->model('TemaModel')->updateDataTema($_POST) > 0 ) {
            Flasher::setMessage('Berhasil','diupdate','success');
            header('Location: '. base_url . '/tema');
            exit;			
        }else{
            Flasher::setMessage('Gagal','diupdate','danger');
            header('Location: '. base_url . '/tema');
            exit;	
        }
    }

    public function hapus($id){
		if( $this->model('TemaModel')->deleteTema($id) > 0 ) {
			Flasher::setMessage('Berhasil','dihapus','success');
			header('Location: '. base_url . '/tema');
			exit;			
		}else{
			Flasher::setMessage('Gagal','dihapus','danger');
			header('Location: '. base_url . '/tema');
			exit;	
		}
	}

}