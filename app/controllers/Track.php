<?php 

    class Track extends Controller{

        public function __construct()
        {	
            if($_SESSION['session_login'] != 'sudah_login') {
                Flasher::setMessage('Login','Tidak ditemukan.','danger');
                header('location: '. base_url . '/login');
                exit;
            }
        }

        public function index()
        {
            $data['title'] = 'Data Track';
            $data['track'] = $this->model('TrackModel')->getAllTrack();
            $data['level'] = $_SESSION['level'];
            $this->view('templates/header', $data);
            $this->view('track/index', $data);
        }

        
        public function tambah(){ 
            $data['title'] = "Tambah Data Track"; 
            $data['tema'] = $this->model('TemaModel')->getAllTema(); 
          
            $this->view('templates/header', $data);
            $this->view('templates/sidebar', $data);
            $this->view('track/tambah', $data);
            $this->view('templates/footer', $data);
        }


        public function simpanData(){
            if($this->model('TrackModel')->tambahTrack($_POST) > 0 ){
                Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); 
                header('Location:'.base_url.'/track' ); 
                exit;
            }else{
                Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); 
                header('Location:'.base_url.'/track' ); 
                exit;
            }
        }

     

    }