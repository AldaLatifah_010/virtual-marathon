<?php

class Login extends Controller {
	
	public function index()
	{
		$data['title'] = 'Halaman Login';
		$this->view('login/login', $data);
	}

	public function register()
	{
		$data['title'] = 'Halaman Register';
		$this->view('login/register', $data);
	}

	public function prosesLogin() {
		
		if($this->model('LoginModel')->checkLogin($_POST) > 0 ) {
				$row = $this->model('LoginModel')->checkLogin($_POST);
				
				$_SESSION['username'] = $row["username"];
				$_SESSION['nama'] = $row["nama"];
				$_SESSION['level'] = $row["level"];
				$_SESSION['session_login'] = 'sudah_login';
				// var_dump($_SESSION['session_login']);
				// die;
				//$this->model('LoginModel')->isLoggedIn($_SESSION['session_login']);
				
				header('Location: '. base_url . '/home');
		} else {
			Flasher::setMessage('Username / Password','salah.','danger');
			header('Location: '. base_url . '/login');
			exit;	
		}
	}

	public function tambahRegister(){
		if($this->model('LoginModel')->tambahUser($_POST) > 0 ){
			Flasher::setMessage('Berhasil', 'ditambahkan', 'success'); 
			header('Location:'.base_url.'/login' ); 
			exit;
		}else{
			Flasher::setMessage('Gagal', 'ditambahkan', 'danger'); 
			header('Location:'.base_url.'/login' ); 
			exit;
		}
	}
}