<?php 

    class Route{
        public function get(string $routename, $callback)
        {
          if ($this->get_uri() == $routename) {
              if (isset($_GET)) {
                if (is_string($callback)) {
                    echo $this->string_handler($callback);
                }else{
                    $callback($callback);
                }
              }
          }
        }

        public function post(string $routename, $callback)
        {
          if ($this->get_uri() == $routename) {
              if (isset($_POST)) {
                if (is_string($callback)) {
                    echo $this->string_handler($callback);
                }else{
                    $callback($callback);
                }
              }
          }
        }

       

        public function string_handler(string $string)
        {
            if (strpos($string,"@")) {
                return $this->class_handler($string);
            }else{
                return $string;
            }
        }

        public function class_handler($string)
        {
            $exp = explode("@", $string);
            $className = $exp[0];
            $functionName = $exp[1];
            require "../app/controllers/$className.php";
            $class = new $className;
            return $class->$functionName();
        }

        public function get_uri(){
            $uri = $_SERVER['REQUEST_URI'];
            $exp = explode("/", $uri);
            $uri = $uri[2];
            if($uri = "virtual-marathon"){
                $uri = $exp[3];
            }
            $uri = (strpos($uri, "?")) ? substr($uri, 0, strpos($uri, "?")) : $uri;

            return $uri;
        }
    }


?>