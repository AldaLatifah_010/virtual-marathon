<?php

if( !session_id() ) session_start();

require_once '../app/init.php';

$route = new Route;

//route tampil halaman
$route->get('','Profil@index');
$route->get('login','Login@index');
$route->get('register', 'Login@register');
$route->get('home', 'Home@index');
$route->get('tema', 'Tema@index');
$route->get('track', 'Track@index');
//route tampil halaman per session



//route post data
$route->post('prosesLogin','Login@prosesLogin');



